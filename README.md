⚠ Deprecated project ⚠: after reading [Discussion on the xwiki-users list - Aug. 2016](http://lists.xwiki.org/pipermail/users/2016-August/033474.html) we decide that we will no longer use this extension and no longer maintain this project.

Authenticator converting LDAP UIDs to lower case when creating XWiki user documents.

## Installation

* Install Maven > 3.1.1 and configure its settings as described at http://dev.xwiki.org/xwiki/bin/view/Community/Building
* Run `mvn clean install`
* A generated jar should be available in the following folder: `~/.m2/repository/org/xwiki/contrib/ldap/ldap-authenticator-lowercase-uid/1.0/`
* In 'xwiki.properties', uncomment the following line: `extension.repositories=local:maven:file://${sys:user.home}/.m2/repository` so that extensions are searched locally.
* Restart XWiki.
* Head to the extension section of the administration module, click 'Advanced search', enter the exact name and version of the extension: name: org.xwiki.contrib.ldap:ldap-authenticator-lowercase-uid, version: 1.0
* Then click 'Install on farm' and complete the installation process.
* Configure the authentication class parameter of 'xwiki.cfg' as follows: `xwiki.authentication.authclass=org.xwiki.contrib.ldap.LDAPAuthServiceLowercaseUIDImpl`
* Restart XWiki

## Todos

* Consider submitting this extension to xwiki-contrib if meaningful for others

## References

* [Discussion on the xwiki-users list - Aug. 2016](http://lists.xwiki.org/pipermail/users/2016-August/033474.html)
* [XWiki:JIRA:LDAP:Let admins decide how to generate XWiki user ids](http://jira.xwiki.org/browse/LDAP-21)
* Custom LDAP authenticator example: [xwiki-contrib/xwiki-authenticator-trusted-ldap](https://github.com/xwiki-contrib/xwiki-authenticator-trusted-ldap)
