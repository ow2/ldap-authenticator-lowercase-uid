/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.xwiki.contrib.ldap;

import java.io.UnsupportedEncodingException;
import java.security.Principal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.novell.ldap.LDAPException;

import org.xwiki.component.annotation.Component;

import com.xpn.xwiki.XWikiContext;
import com.xpn.xwiki.XWikiException;

/** Authenticator converting all usernames to lowercase.
 * @version $Id$
 */
@Component
public class LDAPAuthServiceLowercaseUIDImpl extends XWikiLDAPAuthServiceImpl
{

    private static final Logger LOGGER = LoggerFactory.getLogger(LDAPAuthServiceLowercaseUIDImpl.class);

    public LDAPAuthServiceLowercaseUIDImpl() {
       super();
       LOGGER.debug("************************* LDAPAuthServiceLowercaseUIDImpl");
    }

    protected Principal ldapAuthenticateInContext(String authInput, String validXWikiUserName, String password,
        boolean trusted, XWikiContext context, boolean local)
        throws XWikiException, UnsupportedEncodingException, LDAPException
    {
        LOGGER.debug("Username: " + validXWikiUserName);
        LOGGER.debug("AuthInput: " + authInput);
        if (authInput != null)
          authInput = authInput.toLowerCase();
        if (validXWikiUserName != null)
          validXWikiUserName = validXWikiUserName.toLowerCase();
        return super.ldapAuthenticateInContext(authInput, validXWikiUserName, password, trusted,
        context, local);
    }
}
